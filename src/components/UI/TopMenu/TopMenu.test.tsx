import React from "react";
import { create } from "react-test-renderer";
import TopMenu from "./TopMenu";
import { StaticRouter } from "react-router";

describe("UI/TopMenu", () => {
  test("should render the menu without errors", () => {
    const context = {};
    const topMenu = create(
      <StaticRouter location="contacts" context={context}>
        <TopMenu />
      </StaticRouter>
    );
    expect(topMenu.toJSON()).toMatchSnapshot();
  });
});
