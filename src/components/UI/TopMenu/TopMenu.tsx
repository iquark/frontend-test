import React from "react";
import { Link } from "react-router-dom";
import "./TopMenu.css";

const TopMenu = () => (
  <nav className="TopMenu">
    <Link className="link" to="/contacts">
      Contacts
    </Link>
    |
    <Link className="link" to="/contacts/new">
      New Contact
    </Link>
  </nav>
);

export default TopMenu;
