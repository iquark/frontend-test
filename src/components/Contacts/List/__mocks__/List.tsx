import React from "react";
import { Props } from "../List";

const List = ({ contacts }: Props) => (
  <div>A mock with '{contacts}' passed!</div>
);
export default List;
