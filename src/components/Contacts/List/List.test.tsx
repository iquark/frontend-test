import React from "react";
import { create } from "react-test-renderer";
import { StaticRouter } from "react-router";
import { ContactState } from "../../../store/contacts/types";
import { List } from "./List";

describe("Contacts/List", () => {
  const data: ContactState = {
    list: [
      { id: 1, name: "Pedro Almodovar", email: "ihavenoemail@email.com" },
      { id: 2, name: "Guy Ritchie", email: "that_rich_guy@email.com" }
    ]
  };

  test("should render a list of contacts", () => {
    const context = {};
    const list = create(
      <StaticRouter location="contacts" context={context}>
        <List contacts={data} />
      </StaticRouter>
    );
    expect(list.toJSON()).toMatchSnapshot();
  });

  test("should render 'No contacts in your list yet' if there are no contacts", () => {
    const context = {};
    const list = create(
      <StaticRouter location="contacts" context={context}>
        <List contacts={{ list: [] }} />
      </StaticRouter>
    );
    expect(list.toJSON()).toMatchSnapshot();
  });
});
