import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { State } from "../../../store/types";
import { ContactState } from "../../../store/contacts/types";
import "./List.css";

export interface Props {
  contacts: ContactState;
}

export const List = ({ contacts }: Props) => {
  return (
    <ul className="List">
      {!!contacts && !!contacts.list.length ? (
        contacts.list.map(c => (
          <li key={c.id}>
            <Link to={`/contact/${c.id}`}>{c.name}</Link>
          </li>
        ))
      ) : (
        <li>No contacts in your list yet</li>
      )}
    </ul>
  );
};

const mapStateToProps = (state: State) => ({
  contacts: state.contacts
});

export default connect(mapStateToProps)(List);
