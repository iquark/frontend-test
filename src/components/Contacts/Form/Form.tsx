import React, { SyntheticEvent, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Contact } from "../../../store/contacts/types";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { addContact, editContact } from "../../../store/contacts/actions";
import { Redirect } from "react-router-dom";

export interface Props {
  contact?: Contact;
  addContact(name: string, email: string): void;
  editContact(contact: Contact): void;
}

export const Form = ({ addContact, editContact, contact }: Props) => {
  const [values, setValues] = useState({
    id: -1,
    name: "",
    email: ""
  });
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    if (!!contact) {
      setValues(contact);
    }
  }, []);

  const handleChange = (name: string) => (event: SyntheticEvent) => {
    setValues({
      ...values,
      [name]: (event.target as HTMLInputElement).value
    });
  };

  const onSubmit = (evt: SyntheticEvent) => {
    evt.preventDefault();
    if (!contact) {
      addContact(values.name, values.email);
    } else {
      editContact(values);
    }
    // I know, it does not perform any validation, but there are time restrictions
    setFinished(true);
  };
  return (
    <form onSubmit={onSubmit}>
      <TextField
        id="name"
        label="Name"
        value={values.name}
        onChange={handleChange("name")}
        margin="normal"
      />
      <TextField
        id="email"
        label="Email"
        value={values.email}
        onChange={handleChange("email")}
        margin="normal"
      />
      <div>
        <Button type="submit" variant="outlined" color="primary">
          {!contact ? "Create" : "Update"}
        </Button>
      </div>
      {finished && <Redirect to="/contacts" />}
    </form>
  );
};

const mapStateToProps = null;

const mapDispatchToProps = (dispatch: any) => ({
  addContact: (name: string, email: string) =>
    dispatch(addContact(name, email)),
  editContact: (contact: Contact) => dispatch(editContact(contact))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);
