import React from "react";
import { create } from "react-test-renderer";
import { StaticRouter } from "react-router";
import { Form } from "./Form";
import { Contact } from "../../../store/contacts/types";

describe("Contacts/Form", () => {
  let addContact: any, editContact: any;
  const event: any = {
    preventDefault: () => {}
  };
  const context: any = {};

  beforeEach(() => {
    addContact = jest.fn();
    editContact = jest.fn();
  });

  describe("Creation form", () => {
    test("should render the contact creation form if there is no contact to edit", () => {
      const form = create(
        <Form addContact={addContact} editContact={editContact} />
      );
      expect(form.toJSON()).toMatchSnapshot();
    });
    test("should call the 'addContact' action when the user click on Submit", () => {
      const form = create(
        <StaticRouter location="contacts" context={context}>
          <Form addContact={addContact} editContact={editContact} />
        </StaticRouter>
      );
      const instance = form.root;
      expect(instance).not.toBeFalsy();
      if (instance) {
        const form = instance.findByType("form");
        form.props.onSubmit(event);
        expect(addContact).toHaveBeenCalled();
      }
    });
  });

  describe("Edition form", () => {
    const contact: Contact = {
      id: 12,
      name: "Pepe",
      email: "pepe@email.com"
    };
    test("should render the contact creation form if there is contact to edit", () => {
      const form = create(
        <Form
          contact={contact}
          addContact={addContact}
          editContact={editContact}
        />
      );
      expect(form.toJSON()).toMatchSnapshot();
    });
    test("should call the 'editContact' action when the user click on Submit", () => {
      const form = create(
        <StaticRouter location="contacts" context={context}>
          <Form
            contact={contact}
            addContact={addContact}
            editContact={editContact}
          />
        </StaticRouter>
      );
      const instance = form.root;
      expect(instance).not.toBeFalsy();
      if (instance) {
        const form = instance.findByType("form");
        form.props.onSubmit(event);
        expect(editContact).toHaveBeenCalled();
      }
    });
  });
});
