import React from "react";
import { create } from "react-test-renderer";
import { StaticRouter } from "react-router";
import { Contact } from "../../../store/contacts/types";
import { View } from "./View";

describe("Contacts/View", () => {
  let deleteContact: any;
  const data: Contact = {
    id: 1,
    name: "Pedro Almodovar",
    email: "ihavenoemail@email.com"
  };

  beforeEach(() => {
    deleteContact = jest.fn();
  });

  test("should render the view of a contact", () => {
    const context = {};
    const view = create(
      <StaticRouter location="contacts" context={context}>
        <View contact={data} deleteContact={deleteContact} />
      </StaticRouter>
    );
    expect(view.toJSON()).toMatchSnapshot();
  });

  test("should delete the contact if the user pressed the Delete button", () => {
    const context = {};
    const view = create(
      <StaticRouter location="contacts" context={context}>
        <View contact={data} deleteContact={deleteContact} />
      </StaticRouter>
    );
    const instance = view.root;
    expect(instance).not.toBeFalsy();
    if (instance) {
      const button = instance.findByType("button");
      button.props.onClick();
      expect(deleteContact).toHaveBeenCalled();
    }
  });
});
