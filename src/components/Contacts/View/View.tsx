import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Contact } from "../../../store/contacts/types";
import { deleteContact } from "../../../store/contacts/actions";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

export interface Props {
  contact: Contact;
  deleteContact(id: number): void;
}

export const View = ({ contact, deleteContact }: Props) => {
  const onDelete = (): void => {
    deleteContact(contact.id);
  };

  const MyLink = (props: any) => {
    return <Link to={`/contact/${contact.id}/edit`} {...props} />;
  };

  return (
    <Card>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          {contact.name}
        </Typography>
        <Typography variant="h5" component="h2">
          {contact.email}
        </Typography>
        <Typography color="textSecondary">ID: {contact.id}</Typography>
        <Typography color="textSecondary">
          Creation Date: {contact.creationDate}
        </Typography>
        {!!contact.modifiedDate && (
          <Typography color="textSecondary">
            Modified Date: {contact.modifiedDate}
          </Typography>
        )}
      </CardContent>
      <CardActions>
        <Button color="primary" size="small" component={MyLink}>
          Edit
        </Button>
        <Button color="secondary" size="small" onClick={onDelete}>
          Delete
        </Button>
      </CardActions>
    </Card>
  );
};

const mapStateToProps = null;

const mapDispatchToProps = (dispatch: any) => ({
  deleteContact: (id: number) => dispatch(deleteContact(id))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(View);
