import { getTimestamp } from "./time";

test("should return a timestamp in string type", () => {
  expect(typeof getTimestamp()).toBe("string");
});
