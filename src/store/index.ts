import { contacts } from "./contacts/reducers";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({
  contacts
});

export type AppState = ReturnType<typeof rootReducer>;
