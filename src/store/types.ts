import { ContactState } from "./contacts/types";

export interface State {
  contacts: ContactState;
}
