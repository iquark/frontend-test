import {
  ADD_CONTACT,
  EDIT_CONTACT,
  DELETE_CONTACT,
  LOAD_CONTACT,
  UNLOAD_CONTACT,
  Contact
} from "./types";

export const addContact = (name: string, email: string) => ({
  type: ADD_CONTACT,
  payload: { name, email }
});

export const editContact = (contact: Contact) => ({
  type: EDIT_CONTACT,
  payload: contact
});

export const deleteContact = (id: number) => ({
  type: DELETE_CONTACT,
  payload: { id }
});

export const loadContact = (id: number) => ({
  type: LOAD_CONTACT,
  payload: { id }
});

export const unloadContact = () => ({
  type: UNLOAD_CONTACT
});
