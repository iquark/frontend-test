// constants
export const ADD_CONTACT: string = "CONTACTS/ADD_CONTACT";
export const DELETE_CONTACT: string = "CONTACTS/DELETE_CONTACT";
export const EDIT_CONTACT: string = "CONTACTS/EDIT_CONTACT";
export const LOAD_CONTACT: string = "CONTACTS/LOAD_CONTACT";
export const UNLOAD_CONTACT: string = "CONTACTS/UNLOAD_CONTACT";

// interfaces
export interface Person {
  name: string;
  email: string;
}

export interface ContactId {
  id: number;
}
export interface Contact {
  id: number;
  name: string;
  email: string;
  creationDate?: string;
  modifiedDate?: string;
}

export interface ContactState {
  list: Contact[];
  viewing?: Contact | null;
}

interface AddContactAction {
  type: typeof ADD_CONTACT;
  payload: Person;
}

interface DeleteContactAction {
  type: typeof DELETE_CONTACT;
  payload: ContactId;
}

interface LoadContactAction {
  type: typeof LOAD_CONTACT;
  payload: ContactId;
}

interface UnloadContactAction {
  type: typeof UNLOAD_CONTACT;
  payload?: void;
}

interface EditContactAction {
  type: typeof EDIT_CONTACT;
  payload: Contact;
}

// types
export type ContactActionTypes =
  | AddContactAction
  | DeleteContactAction
  | EditContactAction
  | LoadContactAction
  | UnloadContactAction;
