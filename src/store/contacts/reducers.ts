import {
  ContactState,
  ContactActionTypes,
  Person,
  ContactId,
  Contact,
  ADD_CONTACT,
  DELETE_CONTACT,
  LOAD_CONTACT,
  UNLOAD_CONTACT,
  EDIT_CONTACT
} from "./types";
import { getTimestamp } from "../../helpers/time";

const initialState: ContactState = {
  list: []
};

export function contacts(
  state = initialState,
  action: ContactActionTypes
): ContactState {
  switch (action.type) {
    case ADD_CONTACT:
      return {
        list: [
          ...state.list,
          {
            ...(action.payload as Person),
            id: state.list.length,
            creationDate: getTimestamp()
          }
        ]
      };
    case LOAD_CONTACT:
      return {
        ...state,
        viewing: state.list.find(c => c.id === (action.payload as ContactId).id)
      };
    case UNLOAD_CONTACT:
      return {
        ...state,
        viewing: undefined
      };
    case EDIT_CONTACT:
      return {
        list: state.list.map(contact => {
          if (contact.id === (action.payload as Contact).id) {
            return {
              ...(action.payload as Contact),
              modifiedDate: getTimestamp()
            };
          }
          return contact;
        })
      };
    case DELETE_CONTACT:
      return {
        list: state.list.filter(
          contact => contact.id !== (action.payload as ContactId).id
        )
      };
    default:
      return state;
  }
}
