import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import TopMenu from "../../components/UI/TopMenu/TopMenu";
import ListScreen from "../Contacts/List/ListScreen";
import CreateScreen from "../Contacts/Create/CreateScreen";
import ViewScreen from "../Contacts/View/ViewScreen";
import EditScreen from "../Contacts/Edit/EditScreen";
import "./App.css";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <TopMenu />
          <Route path="/contacts" exact component={ListScreen} />
          <Route path="/contacts/new" exact component={CreateScreen} />
          <Route path="/contact/:id" exact component={ViewScreen} />
          <Route path="/contact/:id/edit" exact component={EditScreen} />
        </div>
      </Router>
    );
  }
}

export default App;
