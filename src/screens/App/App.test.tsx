import React from "react";
import { create } from "react-test-renderer";
import App from "./App";

describe("Screens/App", () => {
  test("should render the menu without errors", () => {
    const list = create(<App />);
    expect(list.toJSON()).toMatchSnapshot();
  });
});
