import React from "react";
import { create } from "react-test-renderer";
import CreateScreen from "./CreateScreen";
import { StaticRouter } from "react-router";
import { Contact } from "../../../store/contacts/types";
jest.mock("../../../components/Contacts/Form/Form");

describe("Screens/CreateScreen", () => {
  test("should render the screen without errors", () => {
    const createScreen = create(<CreateScreen />);
    expect(createScreen.toJSON()).toMatchSnapshot();
  });
});
