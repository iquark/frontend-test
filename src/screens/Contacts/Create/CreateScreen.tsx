import React from "react";
import Form from "../../../components/Contacts/Form/Form";
import "./CreateScreen.css";

const CreateScreen = () => (
  <div>
    <h1>Create a new contact</h1>
    <Form />
  </div>
);

export default CreateScreen;
