import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import View from "../../../components/Contacts/View/View";
import { RouteComponentProps } from "react-router-dom";
import { Contact } from "../../../store/contacts/types";
import { State } from "../../../store/types";
import "./ViewScreen.css";
import { loadContact, unloadContact } from "../../../store/contacts/actions";

export type TParams = { id: string };

interface Props extends RouteComponentProps<TParams> {
  contact?: Contact | null;
  loadContact(id: string): void;
  unloadContact(): void;
}

export const ViewScreen = ({
  match,
  contact,
  loadContact,
  unloadContact
}: Props) => {
  const [found, setFound] = useState(false);

  useEffect(() => {
    loadContact(match.params.id);

    return () => {
      unloadContact();
    };
  }, []);

  useEffect(() => {
    if (!!contact && !found) {
      setFound(true);
    }
  }, [contact]);

  return (
    <div>
      <h1>View contact</h1>
      <div className="View-container">
        {!!contact ? <View contact={contact} /> : <div>Loading contact</div>}
      </div>
      {!contact && found && <Redirect to="/contacts" />}
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  contact: state.contacts.viewing
});

const mapDispatchToProps = (dispatch: any) => ({
  loadContact: (id: string) => {
    dispatch(loadContact(parseInt(id, 10)));
  },
  unloadContact: () => dispatch(unloadContact())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewScreen);
