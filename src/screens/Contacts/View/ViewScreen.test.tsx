import React from "react";
import { create } from "react-test-renderer";
import { ViewScreen } from "./ViewScreen";
import { StaticRouter } from "react-router";
import { Contact } from "../../../store/contacts/types";
jest.mock("../../../components/Contacts/View/View");

describe("Screens/ViewScreen", () => {
  let loadContact: any, unloadContact: any;
  const contact: Contact = { id: 10, name: "pepe", email: "abc@acb.com" };
  beforeEach(() => {
    loadContact = jest.fn();
    unloadContact = jest.fn();
  });

  test("should render the screen without errors", () => {
    const context = {};
    const mock: any = jest.fn();
    const list = create(
      <StaticRouter location="contacts" context={context}>
        <ViewScreen
          match={mock}
          location={mock}
          history={mock}
          loadContact={loadContact}
          unloadContact={unloadContact}
          contact={contact}
        />
      </StaticRouter>
    );
    expect(list.toJSON()).toMatchSnapshot();
  });
});
