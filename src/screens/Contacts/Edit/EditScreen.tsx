import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Form from "../../../components/Contacts/Form/Form";
import { RouteComponentProps } from "react-router-dom";
import { Contact } from "../../../store/contacts/types";
import { State } from "../../../store/types";
import "./EditScreen.css";
import { loadContact, unloadContact } from "../../../store/contacts/actions";

export type TParams = { id: string };

interface Props extends RouteComponentProps<TParams> {
  contact?: Contact | null;
  loadContact(id: string): void;
  unloadContact(): void;
}

export const EditScreen = ({
  match,
  contact,
  loadContact,
  unloadContact
}: Props) => {
  const [found, setFound] = useState(false);

  useEffect(() => {
    loadContact(match.params.id);

    return () => {
      unloadContact();
    };
  }, []);

  useEffect(() => {
    if (!contact && !found) {
      setFound(true);
    }
  }, [contact]);

  if (!!contact && !found) {
    setFound(true);
  }

  return (
    <div>
      <h1>Edit contact</h1>
      <div className="Edit-container">
        {!!contact ? <Form contact={contact} /> : <div>Loading contact</div>}
        {!contact && found && <Redirect to="/contacts" />}
      </div>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  contact: state.contacts.viewing
});

const mapDispatchToProps = (dispatch: any) => ({
  loadContact: (id: string) => {
    dispatch(loadContact(parseInt(id, 10)));
  },
  unloadContact: () => dispatch(unloadContact())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditScreen);
