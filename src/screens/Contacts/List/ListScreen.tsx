import React from "react";
import List from "../../../components/Contacts/List/List";
import "./ListScreen.css";

const ListScreen = () => (
  <div>
    <h1>Contacts</h1>
    <List />
  </div>
);

export default ListScreen;
