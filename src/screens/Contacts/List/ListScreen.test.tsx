import React from "react";
import { create } from "react-test-renderer";
import ListScreen from "./ListScreen";
jest.mock("../../../components/Contacts/List/List");

describe("Screens/ListScreen", () => {
  test("should render the screen without errors", () => {
    const list = create(<ListScreen />);
    expect(list.toJSON()).toMatchSnapshot();
  });
});
